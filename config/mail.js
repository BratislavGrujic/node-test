const nodemailer = require('nodemailer');

// nodemailer settings
const transport = nodemailer.createTransport({
  host: 'smtp.mailtrap.io',
  port: 587,
  secure: false,
  auth: {
    user: 'd01e790533a562',
    pass: '16c19e558e0843'
  },
  tls: { rejectUnauthorized: false }
});

module.exports = {
  // send mail function
  sendEmail(from, to, subject, html, callback) {
    transport.sendMail({
      from,
      to,
      subject,
      html
    }, (err, info) => {
      if (err) {
        console.log(`Error: ${err}`);
      }
      else {
        callback();
      }
    })
  }
};