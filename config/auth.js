module.exports = {
  // Checking if user is logged in
  ensureAuthenticated: (req, res, next) => {
    if(req.isAuthenticated()) {
      return next();
    }
    res.redirect('/user/login');
  },
   // Checking if user is admin
  isAdmin: (req,res,next) => {   
    if(typeof req.user !== 'undefined' && req.user.isAdmin) {
      return next();
    }
    res.redirect('/');
  }
}