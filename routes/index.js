const express = require('express');
const indexController = require('../controllers/index');

const router = express.Router();

// homepage route
router.get('/', indexController.homepage_index);

module.exports = router;