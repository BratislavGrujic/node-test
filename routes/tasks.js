const express = require('express');
const taskController = require('../controllers/task');

const { ensureAuthenticated } = require('../config/auth');

const router = express.Router();

// All task get route
router.get('/all', taskController.task_all_get);

// Create task get route
router.get('/create', ensureAuthenticated, taskController.task_create_get);

// Create task post route
router.post('/create', taskController.task_create_post);

// Single task get route
router.get('/:id', ensureAuthenticated, taskController.task_single_get);

// Update taks
router.post('/update/:id', taskController.update_single_task_post);

// soft delete task
router.post('/softdelete/:id', taskController.delete_single_task_post);

// delete from db task
router.delete('/:id', taskController.task_admin_delete);

module.exports = router;