const express = require('express');
const adminController = require('../controllers/admin');
const { isAdmin } = require('../config/auth');

const router = express.Router();

// dashboard admin route
router.get('/dashboard', isAdmin, adminController.dashboard_page_get);

// activate regular user with admin
router.post('/activate/:id', isAdmin, adminController.activate_user_post);

module.exports = router;