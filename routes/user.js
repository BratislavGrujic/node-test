const express = require('express');
const userController = require('../controllers/user');

const router = express.Router();

// Register user post route
router.post('/register', userController.user_register_post);

// Verification after register route
router.get('/verification', userController.verification_email_get);

// Register user get route
router.get('/register', userController.user_register_get);

// Login user get route
router.get('/login', userController.user_login_get);

// Login user post route
router.post('/login', userController.user_login_post);

// Logout user post route
router.get('/logout', userController.user_logout_post);

// verify email route
router.get('/verify/:token', userController.email_verify_get);

// user delete route
router.delete('/:id', userController.user_delete);

module.exports = router;