const express = require('express');
const mongoose = require('mongoose');
const indexRoute = require('./routes/index');
const userRoute = require('./routes/user');
const passport = require('passport');
const tasksRoute = require('./routes/tasks');
const adminRoute = require('./routes/admin');
const session = require('express-session');
const flash = require('connect-flash');

// Passport config
require('./config/passport')(passport);

// Express app
const app = express();

const port = 3000;

// Connect to mongodb
const dbURI = 'mongodb+srv://grujan:qazxsw123@cluster0.lytqk.mongodb.net/quantoxTest?retryWrites=true&w=majority';
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => app.listen(port, () => console.log(`listening on port: ${port}`)))
  .catch(err => console.log(err));

// Register view engine
app.set('view engine', 'ejs');

// Middleware & static files
app.use(express.urlencoded({ extended: true}));
app.use(express.static('public'));

app.use(
  session({
    secret: 'secretKeyWhatever',
    resave: false,
    saveUninitialized: false
  })
);

// Passport middlware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

//Globals
app.use((req, res, next) => {
  res.locals.isloggedIn = req.isAuthenticated();
  res.locals.error = req.flash('error');
  next();
});

//Routes
app.use('/', indexRoute);
app.use('/user', userRoute);
app.use('/tasks', tasksRoute);
app.use('/admin', adminRoute);

// 404 page
app.use((req, res) => {
  res.status(404).render('404', {user: req.user});
});