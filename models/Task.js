const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  ticketAssigned: {
    type: String,
    required: false
  },
  isDeleted: {
    type: Boolean,
    default: false
  }
},{ timestamps: true })

const Task = mongoose.model('Task', TaskSchema);

module.exports = Task;