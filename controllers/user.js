const User = require('../models/User');
const bcrypt = require('bcryptjs');
const passport = require('passport');
const randomstring = require('randomstring');
const mail = require('../config/mail');

// register form method function handler
const user_register_post = (req, res) => {
  const { username, email, password, confirmPassword } = req.body;
  let errors = [];
  // Check required fields
  if(!username || !email || !password) {
    errors.push({ msg: 'Please fill in all fields'});
  }

  // Check passwords match
  if(password !== confirmPassword) {
    errors.push({ msg: 'Passwords do not match'});
  }

  // Check password length
  if(password.length < 6) {
    errors.push({ msg: 'Password should be a least 6 characters'});
  }

  if(errors.length > 0) {
    res.render('register', { 
      errors, 
      username,
      email,
      password,
      confirmPassword 
    });
  } else {
    // Validation passed
    User.findOne({ email })
      .then(user => {
        if(user) {
          // User exists
          errors.push({ msg: 'Email is already registered'});
          res.render('register', { 
            errors, 
            username,
            email,
            password,
            confirmPassword 
          })
        } else {
          const newUser = new User({
            username,
            email,
            password
          });
          // Hash password
          bcrypt.genSalt(10, (err, salt) => bcrypt.hash(newUser.password, salt, (err, hash) => {
            if(err) throw err;
            // Set password to hash
            newUser.password = hash;

            // Generate secret token
            const secretToken = randomstring.generate();
            newUser.secretToken = secretToken;

            // Save user
            newUser.save()
              .then(user => {
                const html = `Hi there,
                <br/>
                Thank you for registering!
                <br/><br/>
                Please click on the link to verify your account
                <a href=http://localhost:3000/user/verify/${secretToken}>http://localhost:3000/user/verify/${secretToken}</a>`;
                mail.sendEmail('bracog13@gmail.com', email, 'Please verify your email', html, res.redirect('/user/verification'));
              })
              .catch(err => console.log(err));
          }))
        }
      })
  }
}

// login form method function handler
const user_login_post = (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/tasks/all',
    failureRedirect: '/user/login',
    failureFlash: true,
  })(req, res, next);
}

// register page handler
const user_register_get = (req, res) => {
  res.render('register');
}

// verification email page handler
const verification_email_get = (req, res) => {
  res.render('verificationEmailSent');
}

// user login page handler
const user_login_get = (req, res) => {
  res.render('login');
}

// user logout handler
const user_logout_post = (req, res) => {
  req.logout();
  res.redirect('/');
}

// link from the email handler
const email_verify_get = (req, res) => {
  const secretToken = req.params.token;
  User.findOne({ secretToken })
    .then(user => {
      if(!user) {
        res.redirect('/user/login');
      }
      user.secretToken = '';
      user.isEmailValidated = true;
      user.save(err => {
        if(err) console.log(err);
        res.render('verifyEmail');
      });
    })
    .catch(err => console.log(err));
}

const user_delete = (req, res) => {
  const id = req.params.id;
  User.findByIdAndDelete(id)
    .then(() => {
      res.render('adminDashboard',{ user: req.user });
    })
    .catch(err => {
      console.log(err);
    });
}

module.exports = {
  user_register_post,
  verification_email_get,
  user_register_get,
  user_login_get,
  user_login_post,
  user_logout_post,
  email_verify_get,
  user_delete
}