const Task = require('../models/Task');
const User = require('../models/User');

const task_all_get = (req, res) => {
  Task.find().sort({ createdAt: -1 })
    .then(result => {
      res.render('allTasks', { tasks: result, user: req.user });
    })
    .catch(err => {
      console.log(err);
    });
}

const task_create_get = (req, res) => {
  User.find().then(users => {
    res.render('createTask', { users, user: req.user })
  });
}

const task_create_post = (req, res) => {
  const { title, content, status, ticketAssigned } = req.body;
  const newTask = new Task({
    title, 
    content, 
    status, 
    email: req.user.email, 
    ticketAssigned
  });
  newTask.save().then(() => {
    res.redirect('/tasks/all');
  })
  .catch(err => console.log(err));;
}

const task_single_get = (req, res) => {
  const id = req.params.id;
  Task.findById(id)
    .then(result => {
      User.find().then(users => {
        res.render('singleTask', { task: result, user: req.user, users });
      });
    })
    .catch(err => {
      res.render('404');
      console.log(err);
    });
}

const update_single_task_post = (req, res) => {
  const { title, status, content, email, ticketAssigned } = req.body;
  const id = req.params.id;
  Task.findById(id)
    .then(task => {
      // checking if there is new value and if its not using the old one
      task.title = typeof title !== "undefined" ? title : task.title;
      task.status = typeof status !== "undefined" ? status : task.status ;
      task.content = typeof content !== "undefined" ? content : task.content;
      task.email = typeof email !== "undefined" ? email : task.email;
      task.ticketAssigned = typeof ticketAssigned !== "undefined" ? ticketAssigned : task.ticketAssigned;
      task.save(err => {
        if(err) console.log(err);
        res.redirect('/tasks/all');
      });
    })
    .catch(err => {
      res.render('404');
      console.log(err);
    });
}

const delete_single_task_post = (req, res) => {
  const id = req.params.id;
  Task.findById(id)
  .then(task => {
    // setting a soft delete
    task.isDeleted = true;
    task.save(err => {
      if(err) console.log(err);
      res.redirect('/tasks/all');
    });
  })
  .catch(err => {
    res.render('404');
    console.log(err);
  });
}

const task_admin_delete = (req, res) => {
  const id = req.params.id;
  Task.findByIdAndDelete(id)
    .then(() => {
      res.render('adminDashboard',{ user: req.user });
    })
    .catch(err => {
      console.log(err);
    });
}

module.exports = {
  task_all_get,
  task_create_get,
  task_create_post,
  task_single_get,
  update_single_task_post,
  delete_single_task_post,
  task_admin_delete
};