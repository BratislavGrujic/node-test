const homepage_index = (req, res) => {
  res.render('homepage', {user: req.user});
}

module.exports = {
  homepage_index
}