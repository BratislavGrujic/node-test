const User = require('../models/User');
const Task = require('../models/Task');

// admin dashboard page
const dashboard_page_get = (req, res) => {
  User.find().sort({ createdAt: -1 }).then(users => {
    Task.find().then(tasks => res.render('adminDashboard', { user: req.user, usersList: users, deletedTasks: tasks.filter(task => task.isDeleted) })).catch(err => console.log(err));
  }).catch(err => console.log(err));
}

// verify email from admin
const activate_user_post = (req, res) => {
  const id = req.params.id;
  User.findById(id).then(user => {
    user.isEmailValidated = true;
    user.secretToken = '';
    user.save(err => {
      if(err) console.log(err);
      res.redirect('/admin/dashboard');
    });
  })
  .catch(err => console.log(err));
}

module.exports = {
  dashboard_page_get,
  activate_user_post
}